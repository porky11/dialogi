#![deny(missing_docs)]

//! A crate to parse dialog lines from a simple markdown inspired format.

use header_parsing::parse_header;
use thiserror::Error;

use std::{
    collections::{HashMap, HashSet},
    fs::{File, read_dir},
    hash::Hash,
    io::{BufRead, BufReader},
    path::{Path, PathBuf},
};

/// A single dialog line consisting of actions to be advanced when the line is displayed, and the text itself.
#[derive(Clone, Debug)]
pub struct DialogLine<P> {
    /// The text of the dialog line.
    pub text: Box<str>,
    /// The actions advanced by the dialog line, identified by parameters of type `P`.
    pub actions: HashSet<P>,
}

/// A full dialog block consisting of the name of the talker, the lines of the dialog, and some final actions to be advanced when the whole text box is fully displayed.
#[derive(Clone, Debug)]
pub struct DialogBlock<P> {
    /// The speaker name of the dialog block.
    pub name: Box<str>,
    /// The text lines of the dialog block.
    pub lines: Vec<DialogLine<P>>,
    /// The actions advanced by the dialog block, identified by parameters of type `P`.
    pub final_actions: HashSet<P>,
}

impl<P> DialogBlock<P> {
    fn new() -> Self {
        Self {
            name: "".into(),
            lines: Vec::new(),
            final_actions: HashSet::new(),
        }
    }

    fn is_empty(&self) -> bool {
        self.name.is_empty() && self.lines.is_empty() && self.final_actions.is_empty()
    }

    /// The text lines of a dialog block as string references.
    pub fn lines(&self) -> impl Iterator<Item = &str> {
        self.lines.iter().map(|line| line.text.as_ref())
    }
}

/// A parameter to define an action of the dialog.
pub trait DialogParameter: Sized {
    /// The parameter context, which might be important for creation by name.
    type Context;
    /// The method to create a parameter. It might access some context and returns some new dialog parameter or none.
    fn create(name: &str, context: &mut Self::Context) -> Option<Self>;
}

/// A change to define an how actions are applied.
pub trait DialogChange: Sized {
    /// The parameter to apply this change.
    type Parameter: DialogParameter + Clone + Eq + Hash;

    /// Creates the change of the parameter back to the default value.
    fn default_change(parameter: Self::Parameter) -> Self;

    /// Creates a change of the parameter to the specified value.
    fn value_change(
        parameter: Self::Parameter,
        value: &str,
        context: &mut <<Self as DialogChange>::Parameter as DialogParameter>::Context,
    ) -> Self;
}

/// Defines a full sequential dialog as a sequence of dialog blocks.
pub struct DialogSequence<C, P> {
    /// The sequence of dialog blocks.
    pub blocks: Vec<DialogBlock<P>>,
    /// The changes to be applied by the dialog parameters.
    pub changes: HashMap<P, Vec<C>>,
}

/// A trait to parse dialog sequences into, identified by a name.
pub trait DialogMap<C: DialogChange>: Default {
    /// Adds a single sequential dialog into the dialog map.
    fn add(&mut self, key: Vec<Box<str>>, value: DialogSequence<C, C::Parameter>);
}

impl<C: DialogChange> DialogMap<C> for HashMap<Vec<Box<str>>, DialogSequence<C, C::Parameter>> {
    fn add(&mut self, key: Vec<Box<str>>, value: DialogSequence<C, C::Parameter>) {
        self.insert(key, value);
    }
}

impl<C: DialogChange> DialogMap<C> for Vec<DialogSequence<C, C::Parameter>> {
    fn add(&mut self, _key: Vec<Box<str>>, value: DialogSequence<C, C::Parameter>) {
        self.push(value);
    }
}

/// An error type returned when parsing a the dialog structure fails.
#[derive(Debug, Error)]
pub enum ParsingError {
    /// Colon parameters are not allowed to have a value supplied.
    #[error("Colon parameters are not allowed to have a value supplied")]
    ColonParameterWithValues,
    /// Error while opening story file.
    #[error("Error while opening story file: {0}")]
    OpeningError(PathBuf),
    /// Error while reading story file.
    #[error("Error while reading story file: {0}")]
    ReadingError(PathBuf),
    /// Subheader found without a matching header.
    #[error("Subheader found without a matching header")]
    SubheaderWithoutHeader,
    /// Invalid dialog format.
    #[error("Invalid dialog format")]
    InvalidIndentation,
    /// Invalid indentation level.
    #[error("Invalid indentation level")]
    IndentationTooHigh,
    /// Default parameters cannot have a value supplied.
    #[error("Default parameters cannot have a value supplied")]
    DefaultParameterWithValue,
    /// Duplicate definition of change.
    #[error("Duplicate definition of change: {0}")]
    DuplicateDefinitionOfChange(Box<str>),
}

impl<C: DialogChange> DialogSequence<C, C::Parameter> {
    fn new() -> Self {
        Self {
            blocks: Vec::new(),
            changes: HashMap::new(),
        }
    }

    /// Loads a new dialog file from a specified path and returns a new text map.
    ///
    /// If the path is a folder, it will be scanned for `.pk` files and sub folders.
    /// If the path is a file, it will be parsed as directly using the `.pk` parsing format.
    pub fn map_from_path<M: DialogMap<C>>(
        path: &Path,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<M, ParsingError> {
        let mut text_map = M::default();
        Self::fill_map_from_path(path, &mut text_map, context)?;
        Ok(text_map)
    }

    /// Loads a new dialog file from a specified path into the supplied text map.
    ///
    /// If the path is a folder, it will be scanned for `.pk` files and sub folders.
    /// If the path is a file, it will be parsed as directly using the `.pk` parsing format.
    pub fn fill_map_from_path<M: DialogMap<C>>(
        path: &Path,
        text_map: &mut M,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<(), ParsingError> {
        Self::named_fill_map_from_path(path, text_map, Vec::new(), context)
    }

    fn named_fill_map_from_path<M: DialogMap<C>>(
        path: &Path,
        text_map: &mut M,
        default_name: Vec<Box<str>>,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<(), ParsingError> {
        let Ok(dirs) = read_dir(path) else {
            return Self::fill_map_from_file(path, default_name, text_map, context);
        };

        for dir in dirs.flatten() {
            Self::try_fill_submap_from_path(&dir.path(), default_name.clone(), text_map, context)?;
        }

        Ok(())
    }

    fn try_fill_submap_from_path<M: DialogMap<C>>(
        path: &Path,
        mut relative_name: Vec<Box<str>>,
        text_map: &mut M,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<(), ParsingError> {
        let valid_path = path.extension().is_none_or(|e| e == "pk");

        if !valid_path {
            return Ok(());
        }

        let Some(name) = path.file_stem() else {
            return Ok(());
        };

        let Some(name) = name.to_str() else {
            return Ok(());
        };

        relative_name.push(name.into());
        Self::named_fill_map_from_path(path, text_map, relative_name, context)
    }

    fn handle_content_line(
        &mut self,
        line: &str,
        mut current_block: DialogBlock<C::Parameter>,
        path: &mut Vec<Box<str>>,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<DialogBlock<C::Parameter>, ParsingError> {
        if line.trim().is_empty() {
            if !current_block.is_empty() {
                self.blocks.push(current_block);
                current_block = DialogBlock::new();
            }

            return Ok(current_block);
        }

        let mut spaces = 0;
        let mut chars = line.chars();
        let mut c = chars.next().unwrap();
        while c == ' ' {
            spaces += 1;
            c = chars.next().unwrap();
        }
        let first = c;

        if first == '-' {
            if spaces % 2 != 0 {
                return Err(ParsingError::InvalidIndentation);
            }
            let level = spaces / 2;
            if level > path.len() {
                return Err(ParsingError::IndentationTooHigh);
            }
            while path.len() > level {
                path.pop();
            }
            let line = line[(spaces + 1)..].trim();
            let (name_end, value) = line
                .split_once(' ')
                .map_or((line, ""), |(name, value)| (name.trim(), value.trim()));
            let default = name_end.ends_with('!');

            if default && !value.is_empty() {
                return Err(ParsingError::DefaultParameterWithValue);
            }

            let colon_end = name_end.ends_with(':');

            let name_end: Box<str> = if default || colon_end {
                &name_end[0..(name_end.len() - 1)]
            } else {
                name_end
            }
            .into();

            if colon_end {
                if !value.is_empty() {
                    return Err(ParsingError::ColonParameterWithValues);
                }

                path.push(name_end);
                return Ok(current_block);
            }

            let parameter_name = path.iter().rev().fold(name_end.clone(), |name, element| {
                format!("{element}:{name}").into()
            });

            path.push(name_end);

            let Some(parameter) = DialogParameter::create(&parameter_name, context) else {
                return Ok(current_block);
            };

            if current_block.final_actions.contains(&parameter) {
                return Err(ParsingError::DuplicateDefinitionOfChange(parameter_name));
            }

            let change = if default {
                DialogChange::default_change(parameter.clone())
            } else {
                DialogChange::value_change(parameter.clone(), value, context)
            };

            if let Some(map) = self.changes.get_mut(&parameter) {
                map.push(change);
            } else {
                self.changes.insert(parameter.clone(), vec![change]);
            }

            current_block.final_actions.insert(parameter);

            return Ok(current_block);
        }

        path.clear();

        let (Some((name, text)), 0) = (line.split_once(':'), spaces) else {
            current_block.lines.push(DialogLine {
                text: line.trim().into(),
                actions: current_block.final_actions,
            });
            current_block.final_actions = HashSet::new();

            return Ok(current_block);
        };

        let parameters = if !current_block.is_empty() {
            self.blocks.push(current_block);
            HashSet::new()
        } else if !current_block.final_actions.is_empty() {
            current_block.final_actions
        } else {
            HashSet::new()
        };

        let (parameters, lines) = if text.is_empty() {
            (parameters, Vec::new())
        } else {
            (
                HashSet::new(),
                vec![DialogLine {
                    text: text.into(),
                    actions: parameters,
                }],
            )
        };

        Ok(DialogBlock {
            name: name.trim().into(),
            lines,
            final_actions: parameters,
        })
    }

    fn fill_map_from_file<M: DialogMap<C>>(
        path: &Path,
        default_name: Vec<Box<str>>,
        text_map: &mut M,
        context: &mut <C::Parameter as DialogParameter>::Context,
    ) -> Result<(), ParsingError> {
        let Ok(story_file) = File::open(path) else {
            return Err(ParsingError::OpeningError(path.to_path_buf()));
        };
        let mut current_block = DialogBlock::new();
        let mut current_sequence = Self::new();
        let mut name = Vec::new();
        let mut parameter_path = Vec::new();

        for line in BufReader::new(story_file).lines() {
            let Ok(line) = line else {
                return Err(ParsingError::ReadingError(path.to_path_buf()));
            };

            if let Some(success) = parse_header(&mut name, &line) {
                let Ok(changes) = success else {
                    return Err(ParsingError::SubheaderWithoutHeader);
                };

                if !current_block.is_empty() {
                    current_sequence.blocks.push(current_block);
                    current_block = DialogBlock::new();
                }

                if !current_sequence.blocks.is_empty() {
                    let mut new_name = default_name.clone();
                    new_name.extend(changes.path.clone());
                    text_map.add(new_name, current_sequence);
                }
                current_sequence = Self::new();

                changes.apply();

                continue;
            }

            current_block = current_sequence.handle_content_line(
                &line,
                current_block,
                &mut parameter_path,
                context,
            )?;
        }

        if !current_block.is_empty() {
            current_sequence.blocks.push(current_block);
        }

        if !current_sequence.blocks.is_empty() {
            let mut new_name = default_name;
            new_name.extend(name);
            text_map.add(new_name, current_sequence);
        }

        Ok(())
    }
}
