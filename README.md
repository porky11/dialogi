This crate parses a simple text based dialog format into a format to work with.
The dialog format is inspired by markdown.

# Text format

## Headers

Every linear dialog sequence is defined by a header starting with `#`.
Subheaders start with multiple `#`.

A header is allowed to start with up to one `#` more than the previous header.
A header is the subheader of the last header with exactly one `#` less.

The content of every header is meant to be interpreted as a sequential dialog.

Empty headers are ignored.

Example:

```
# Header 1

## Subheader 1

Dialog 1

## Subheader 2

Dialog 2

# Header 2

Dialog 3

## Subheader 1

Dialog 4

## Subheader 2

Dialog 5
```

## General format

A dialog consists of multiple text blocks separated by newlines.

Text blocks are meant to be displayed in a single text box at once.

These are three text blocks of different sizes:

```
Textblock 1, Line 1
Textblock 1, Line 2
Textblock 1, Line 3

Textblock 2

Textblock 3, Line 1
Textblock 3, Line 2
```

## Talkers

Talkers can be added in front of a text block, separated by a colon (`:`).
The talker name might contain spaces. The name is trimmed, though.

The text might begin on the same line after the talker name, but also in the next line.
If no talker is specified, a text block is interpreted to not belong to a talker.
This might be condsidetred to be text by a narraor.

This is a simple dialog between Alice and Bob:

```
Alice: Hi!

Bob: Hi, how are you doing today?
Is everything fine?

Bob seems concerend, while waiting for an answer.

Alice:
Yeah, everything is fine today.
Nice to see you again.
```

Having colons in the text is possilbe by adding colons before the text.
Then the colon in the line won't be interpreted as the separator between the name and the text.

## Events

It's also possible to add events in the text.
Lines starting with a minus (`-`) represent events.

This sets values for specific parameters:

```
- Parameter1 Value1
- Parameter2 Value2
```

This reverts parameters back to their default values:

```
- Parameter1!
- Parameter2!
```

The exact parameter and value names availabe have to be specified by implementing traits of this library.

To specify related parameters, you can use parameter paths, which are separated by colons.
Parameter paths can have hierarchic representations using indentation.
The indentation of each line has to be a multiple of 2 and up to two spaces more than the previous line.

Plain representation:

```
- Path:To:Parameter Value
```

Hierarchic representation:

```
- Path:
  -To:
    - Parameter Value
```

Lines ending on a colon (`:`) won't trigger an event by themselves.
It's as if they were ignored, besides for the path.

This allows setting multiple parameters in similar paths like this:

```
- Long:Path:To:
  - Parameter1 Value1
  - Parameter2 Value2
```

This is short for the following:

```
- Long:Path:To:Parameter1 Value1
- Long:Path:To:Parameter2 Value2
```

Defaults also work with hierarchic paths.
